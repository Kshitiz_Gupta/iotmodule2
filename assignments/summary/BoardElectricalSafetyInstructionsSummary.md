Power Supply
    Output voltage of power supply = Input voltage of board.
    Before turning on power supply every circuit should be connected properly.
    Do not connect power supply without matching the power rating.
    Don't force the connector into the power socket, it may damage the connector.

Handling
    Treat every device like it is energized since it is plugged in or operational.
    Before working keep the board on a flat stable surface.
    Unplug the device before performing any operation on them.
    Keep all electrical circuit contact points enclosed.
    If the board becomes too hot, try to cool it with a external usb fan.
    Never touch electrical equipment when any part of your body is wet.
    Do not touch any sort of metal to the development board.

GPIO
    Find out whether the board runs on 3.3v or 5v logic.
    Always connect the LED or sensors using appropriate resistors.
    To Use 5V peripherals with 3.3V it require a logic level converter.
    Never connect anything greater that 5v to a 3.3v pin.
    Avoid making connections when the board is running.
    Don't plug anything with a high or negative voltage.
    Do not connect a motor directly, use a transistor to drive it .

UART
    Connect Rx & Tx pin of device1 to Tx & Rx pin of device2 respecticely.
    If the device1 works on 5v and device2 works at 3.3v then use the level shifting mechanism(voltage divider).
    Genrally used to communicate with board through USB to TTL connection and does not require protection circuit.
    Senor interfacing using UART might require a protection circuit.

I2C Interface
    While using it with sensors SDA and SDL lines must be protected.
    Protection of these lines is done by using pullup registers on both lines.
    If inbuilt pullup registers used in the board it wont need an external circuit.
    If using bread-board to connect a sensor, use the pullup resistor.

SPI Interface
    Generally, SPI in development boards is in Push-pull mode.
    Push-pull mode does not require any protection circuit.
    If more than one slaves used, it is possible that device2 can "hear" and "respond" to the master's communication with device1.
    To overcome this problem, use a protection circuit with pullup resistors on each Slave Select line(CS).
    Resistors value can be between 1kOhm~10kOhm. Generally 4.7kOhm resistor is used.
