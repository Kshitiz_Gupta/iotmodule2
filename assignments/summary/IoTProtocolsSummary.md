4-20 mA
    -Standard for signal transmission and electronic control in control systems.
    -In current loop of series circuit:-
        Dc power supply --> current signal --> transmitter --> controller --> Dc power supply.
    -The advantage is that the current value does not degrade over long distances.
    -So the current signal remains constant through all components in the loop.
    -As a result, the accuracy of the signal is not affected by a voltage drop in the interconnecting wiring.
    -Volatge Drop
        -Voltage signals sent long distance will degrade in accuracy and develop a voltage drop proportional to length of cable.
        -Accuracy loss of the voltage signal would equal the mA signal value multiplied by the resistance of the wire.
        -The use of 4 mA as a "Live Zero" enhances the signal-to-noise-ratio at low levels.
        -This Live Zero also makes a loop failure more apparent.
        -A nonfunctioning current loop with open termination or connection has zero current flow.
        -Which is outside the valid 4 to 20 mA signal range.
    -Componets: Sensor, Transmitter, Power Source, Loop, Reciever.

Modbus Communication Protocol
    -Use in Programmable Logic Controllers.
    -It is typically used to transmit signals.
        From instrumentation and control devices back to a main controller or data gathering system.
    -The method is used for transmitting information between electronic devices.
    -The device requesting information is called “master” and “slaves” are the devices supplying information.
    -In a standard Modbus network, there is one master and up to 247 slaves, each with a unique slave address from 1 to 247.
    -Communication between a master and a slave occurs in a frame that indicates a function code.
    -The function code identifies the action to perform like to read discrete input, first-in, first-out queue.
    -The slave then responds, based on the function code received.
    -The protocol is commonly used in IoT as a local interface to manage devices.
    -Modbus protocol can be used over 2 interfaces:-
        1)RS485 - called as Modbus RTU
            RS485 is a serial transmission standard. Several RS485 devices can be put on the same bus.
            RS485 is not directly compatible.
                Must use the correct type of interface or the signals won't go through.
            Mainly done through an easy to use an RS485 to USB.
        2)Ethernet - called as Modbus TCP/IP

OPCUA Protocol
    Unified Architecture
        Functional equivalence: All COM OPC Classic specifications are mapped to UA.
        Platform independence: From an embedded micro-controller to cloud-based infrastructure.
        Secure: Encryption, Authentication, Auditing
        Extensible: Ability to add new features without affecting existing applications.
        Comprehensive Information Modeling: For defining complex information.
        Object Oriented, Abstract Base Model
    OPCUA Client
        Data Access
        Alarms & Conditions
        Historical Data Access
    OPCUA Server
        A Publisher-Subscriber model.
        Which a UA server makes configurable subsets of information available to any number of recipients.
